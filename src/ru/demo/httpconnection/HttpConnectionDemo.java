package ru.demo.httpconnection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 * Демонстрация чтения файла по url
 */
public class HttpConnectionDemo {
    public static void main(String[] args) throws IOException {
        URL url = new URL("http://rugby-penza.ru/");
        try (InputStream inputStream = url.openStream()){
            new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))
                    .lines()
                    .forEach(System.out::println);
        }
    }
}
