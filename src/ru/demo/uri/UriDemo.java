package ru.demo.uri;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Демонстрация использования методов класса URI
 *
 */
public class UriDemo {
    public static void main(String[] args) throws URISyntaxException {
        dumpUriComponents(new URI("mailto:ita_1001@mail.ru"));
        dumpUriComponents(new URI("http://docs.oracle.com/javase/tutorial/collections/streams/parallelism.html#concurrent_reduction"));
        System.out.println("--------------------");

        URI base = new URI("http://docs.oracle.com/javase/tutorial/essential/concurrency/atomicvars.html");
        URI resolved = base.resolve("../regex/index.html");
        dumpUriComponents(resolved);

        base = new URI("http://docs.oracle.com/javase/tutorial/essential/concurrency/");
        URI relational = base.relativize(new URI("http://docs.oracle.com/javase/tutorial/essential/concurrency/collections.html"));
        dumpUriComponents(relational);
    }

    /**
     * Выводит составные части <code>uri</code>
     * @param uri унифицированный идентификатор ресурса
     */
    private static void dumpUriComponents(URI uri) {
        System.out.println("\t" + uri+ "");
        System.out.println("Scheme: " + uri.getScheme());
        System.out.println("SchemeSpecificPart: " + uri.getSchemeSpecificPart());
        System.out.println("Authority: " + uri.getAuthority());
        System.out.println("UserInfo: " + uri.getUserInfo());
        System.out.println("Host: " + uri.getHost());
        System.out.println("Port: " + uri.getPort());
        System.out.println("Path: " + uri.getPath());
        System.out.println("Query: " + uri.getQuery());
        System.out.println("Fragment: " + uri.getFragment());



    }
}
